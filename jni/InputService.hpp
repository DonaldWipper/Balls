#ifndef _UTYG_INPUTSERVICE_HPP_
#define _UTYG_INPUTSERVICE_HPP_

#include "Context.hpp"
#include "InputHandler.hpp"
#include "Types.hpp"

#include <android_native_app_glue.h>

namespace utyg {
    class InputService : public InputHandler {
    public:
        InputService(android_app* pApplication,
          const int32_t& pWidth, const int32_t& pHeight);

        float getHorizontal();
        float getVertical();
        float getFirstHorizontal();
        float getFirstVertical();

        void setRefPoint(Location* pTouchReference);
        bool ifStartTouch();



        status start();

    public:
        bool onTouchEvent(AInputEvent* pEvent);

    private:
        android_app* mApplication;
        bool mStartTouch;
        // Input values.
        float mHorizontal, mVertical;
        float mFirstHorizontal, mFirstVertical;

        // Reference point to evaluate touch distance.
        Location* mRefPoint;
        const int32_t& mWidth, &mHeight;
    };
}
#endif
