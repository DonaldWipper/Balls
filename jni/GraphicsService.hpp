#ifndef _UTYG_GRAPHICSSERVICE_HPP_
#define _UTYG_GRAPHICSSERVICE_HPP_


#include "TimeService.hpp"
#include "Types.hpp"


#include <android_native_app_glue.h>
#include <EGL/egl.h>
#include "Log.hpp"
#include "SetBalls.hpp"
#include <vector>

namespace utyg {
    class GraphicsService {
    public:
        GraphicsService(android_app* pApplication,
                        TimeService* pTimeService);
        ~GraphicsService();

        const int32_t& getHeight();
        const int32_t& getWidth();

        status start();
        void setup();
        void stop();
        status update();
        void initBalls(setBalls *pSetBalls);


    protected:


    private:
        android_app* mApplication;
        TimeService* mTimeService;

        // Display properties.
        int32_t mWidth, mHeight;


        EGLDisplay mDisplay;
        EGLSurface mSurface;
        EGLContext mContext;
        setBalls *mSetBalls;
        // Graphics resources.

    };
}
#endif
