#ifndef SWYPELINE_HPP_
#define SWYPELINE_HPP_
#include "Types.hpp"
#include "InputService.hpp"
#include "Consts.hpp"
#include "TimeService.hpp"
#include "Ball.hpp"
#include <vector>


class SwypeLine {
	std::vector<utyg::Location> mLine;
	utyg::TimeService* mTimeService;
	utyg::InputService* mInputService;
	double mTimePrevPoint;
	double mTimeDraw;

public:
	SwypeLine(utyg::Context* pContext);
	bool checkBallIntersect(Ball mBall);
	void addPoint(utyg::Location pNewPoint);
	void removePoint(int pNum);
	void removeFirstPoints(int pCnt);
	void remove();
	void draw();
	void update();
};















#endif
