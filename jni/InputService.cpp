#include "InputService.hpp"
#include "Log.hpp"

#include <android_native_app_glue.h>
#include <cmath>

namespace utyg {
    InputService::InputService(android_app* pApplication,
        const int32_t& pWidth, const int32_t& pHeight) :
        mApplication(pApplication),
        mHorizontal(0.0f), mVertical(0.0f), mFirstHorizontal(0.0f), mFirstVertical(0.0f),
        mRefPoint(NULL), mWidth(pWidth), mHeight(pHeight), mStartTouch(false)
    {}

    float InputService::getHorizontal() {
        return mHorizontal;
    }

    float InputService::getVertical() {
        return mHeight - mVertical;
    }

    float InputService::getFirstHorizontal() {
            return mFirstHorizontal;
        }

        float InputService::getFirstVertical() {
            return mFirstVertical;
        }

    bool InputService::ifStartTouch() {
    	return mStartTouch;
    }

    void InputService::setRefPoint(Location* pTouchReference) {
        mRefPoint = pTouchReference;
        utyg::Log::info("SET REF POINT");
    }

    status InputService::start() {
        Log::info("Starting InputService.");
        if ((mWidth == 0) || (mHeight == 0)) {
            return STATUS_KO;
        }
        return STATUS_OK;
    }

    bool InputService::onTouchEvent(AInputEvent* pEvent) {
#ifdef INPUTSERVICE_LOG_EVENTS
         utyg_Log_debug("AMotionEvent_getAction=%d", AMotionEvent_getAction(pEvent));
         utyg_Log_debug("AMotionEvent_getFlags=%d", AMotionEvent_getFlags(pEvent));
         utyg_Log_debug("AMotionEvent_getMetaState=%d", AMotionEvent_getMetaState(pEvent));
         utyg_Log_debug("AMotionEvent_getEdgeFlags=%d", AMotionEvent_getEdgeFlags(pEvent));
         utyg_Log_debug("AMotionEvent_getDownTime=%lld", AMotionEvent_getDownTime(pEvent));
         utyg_Log_debug("AMotionEvent_getEventTime=%lld", AMotionEvent_getEventTime(pEvent));
         utyg_Log_debug("AMotionEvent_getXOffset=%f", AMotionEvent_getXOffset(pEvent));
         utyg_Log_debug("AMotionEvent_getYOffset=%f", AMotionEvent_getYOffset(pEvent));
         utyg_Log_debug("AMotionEvent_getXPrecision=%f", AMotionEvent_getXPrecision(pEvent));
         utyg_Log_debug("AMotionEvent_getYPrecision=%f", AMotionEvent_getYPrecision(pEvent));
         utyg_Log_debug("AMotionEvent_getPointerCount=%d", AMotionEvent_getPointerCount(pEvent));
         utyg_Log_debug("AMotionEvent_getRawX=%f", AMotionEvent_getRawX(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getRawY=%f", AMotionEvent_getRawY(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getX=%f", AMotionEvent_getX(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getY=%f", AMotionEvent_getY(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getPressure=%f", AMotionEvent_getPressure(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getSize=%f", AMotionEvent_getSize(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getOrientation=%f", AMotionEvent_getOrientation(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getTouchMajor=%f", AMotionEvent_getTouchMajor(pEvent, 0));
         utyg_Log_debug("AMotionEvent_getTouchMinor=%f", AMotionEvent_getTouchMinor(pEvent, 0));
#endif

        if (mRefPoint != NULL) {
        	utyg::Log::info("ZERO NO");
            if (AMotionEvent_getAction(pEvent)
                            == AMOTION_EVENT_ACTION_DOWN) {

            	 utyg::Log::info("TOUCH");
            	mStartTouch = 1;
            	mFirstHorizontal = AMotionEvent_getX(pEvent, 0);
            	mFirstVertical =  AMotionEvent_getY(pEvent, 0);
                mHorizontal = mFirstHorizontal;
                mVertical = mFirstVertical;
                }

            if (AMotionEvent_getAction(pEvent)
                                        == AMOTION_EVENT_ACTION_UP) {
            	mStartTouch = 0;
            }

            if (AMotionEvent_getAction(pEvent)
                                              == AMOTION_EVENT_ACTION_MOVE) {
            	             mHorizontal = AMotionEvent_getX(pEvent, 0);
            	             mVertical = AMotionEvent_getY(pEvent, 0);
                        }
        }
        return true;
    }
}
