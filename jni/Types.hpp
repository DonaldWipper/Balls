#ifndef _UTYG_TYPES_HPP_
#define _UTYG_TYPES_HPP_

#include <stdint.h>

namespace utyg {
    typedef int32_t status;


    struct Location {
    	float mPosX;
    	float mPosY;
        float mRadius;
    };

    struct Segment {
    	Location mLeft;
    	Location mRight;
    };

    const status STATUS_OK   = 0;
    const status STATUS_KO   = -1;
    const status STATUS_EXIT = -2;
}
#endif
