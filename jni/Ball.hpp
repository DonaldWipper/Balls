#ifndef BALL_HPP_
#define BALL_HPP_
#include "Types.hpp"
#include "glues_quad.h"
#include "Consts.hpp"

//#include "RenderText.hpp"


class Ball {
	int mType;
	/* 0 -white
	 * 1 -red
	 * 2 - white - left
	 * 3 - white - right
	 */
	//RenderText *mRenderText;
	float mRotateAngle;
	utyg::Location mPos;
	bool mIsDead;
	bool mTotalCrash; //game over
	double mSpeed;

public:
	Ball();
	//~Ball();
	Ball(const Ball & pBall);
	Ball& operator=(const Ball & pBall);
	utyg::Location getPos();
	int getType();
	bool ifTotalCrash();
	void setTotalCrash();
	void setDeath();
	void setType(int pType);
	void setPos(utyg::Location pPos);
	int checkIntersectLine(utyg::Location mPointA, utyg::Location mPointB);
	void update();
	void draw();
};










#endif
