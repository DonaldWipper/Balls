#include "Ball.hpp"
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <math.h>
#include <stdlib.h>


Ball::Ball() {
	mPos.mPosX = 100;
	mPos.mPosY = 100;
	mPos.mRadius = 50;
	mRotateAngle = 0;
	mIsDead = false;
	mType = rand() % 2;
	mSpeed = utyg::BALLS_MIN_SPEED + utyg::BALLS_DELTA_SPEED * (rand() % 5);
	mTotalCrash = false;
	//mRenderText = new RenderText();;
}

int Ball::getType() {
	return mType;
}


Ball::Ball(const Ball & pBall){
	mPos.mPosX = pBall.mPos.mPosX;
    mPos.mPosY = pBall.mPos.mPosY;
	mPos.mRadius = pBall.mPos.mRadius;
	mRotateAngle = pBall.mRotateAngle;
	mType = pBall.mType;
	mSpeed = pBall.mSpeed;
	mIsDead = false;
	mTotalCrash = false;
}




Ball& Ball::operator=(const Ball & pBall){
	mPos.mPosX = pBall.mPos.mPosX;
    mPos.mPosY = pBall.mPos.mPosY;
	mPos.mRadius = pBall.mPos.mRadius;
	mRotateAngle = pBall.mRotateAngle;
	mType = pBall.mType;
	mSpeed = pBall.mSpeed;
	mIsDead = false;
	mTotalCrash = false;
	return (*this);
}



void Ball::setTotalCrash() {
	mTotalCrash = true;
}

bool Ball::ifTotalCrash() {
	return mTotalCrash;
}

void Ball::update() {
	if(mTotalCrash) {
		return;
	}

    float dX, dY;
    switch(mType) {
	case(0):
		dY = -mSpeed;
	    dX = 0;
	    break;
	case(1):
		dY = -mSpeed;
	    dX = 0;
	    break;
	case(2):
	    dX = mSpeed / sqrt(2.0);
		dY = mSpeed / sqrt(2.0);
		break;
	case(3):
	    dX = -mSpeed / sqrt(2.0);
		dY = mSpeed / sqrt(2.0);
		break;
	}


	mPos.mPosY += dY;
	mPos.mPosX += dX;


    mRotateAngle += 3;

	/*if(mRotateAngle > 360) {
		mRotateAngle -= 360;
	}*/

}


int Ball::checkIntersectLine(utyg::Location mA, utyg::Location mB) {
	int res = 2;
	if((mA.mPosX == mB.mPosX) && (mA.mPosY == mB.mPosY)) { //Если линия - это точка, то говорим, что нет
		return 0;
	}

	float k;  //Тангенс угла наклона

    int mType; //Тип линии
    /*0 - нормальная
     *1 - вертикальная
     *2 - горизонтальная
    */


	if (mB.mPosX != mA.mPosX) { //Если линия не вертикальная
	    k = (mB.mPosY - mA.mPosY) / (mB.mPosX - mA.mPosX);
	    mType = 0;
	} else {
	    mType = 1;
	}
	if(k == 0) {
		mType = 2;
	}



	float b = mB.mPosY - k * mB.mPosX;
    float A, B, C, D, E;
    float x1, x2, y1, y2;

    //В зависимость от тангенаса угла наклона прямой определим тип линии
    switch(mType) {
    case(1): // Вертикальная линия ----
        E = pow(mPos.mRadius, 2) - pow(mA.mPosY - mPos.mPosY, 2);
        if(E <= 0) {
            return 0;
        }

        y1 = y2 = mA.mPosY;
        x1 = sqrt(E) + mPos.mPosX;
        x2 = -sqrt(E) + mPos.mPosX;

    case(2): // Горизонтальная линия |
    	E = pow(mPos.mRadius, 2) - pow(mA.mPosX - mPos.mPosX, 2);
        if(E <= 0) {
            return 0;
        }

        x1 = x2 = mA.mPosX;
        y1 = sqrt(E) + mPos.mPosY;
        y2 = -sqrt(E) + mPos.mPosX;
        break;
    case(0):
    	A = 1 + k * k;
    	B = 2 * (-mPos.mPosX + k * (b - mPos.mPosY));
    	C = pow(mPos.mPosX, 2) + pow(b - mPos.mPosY, 2) - pow(mPos.mRadius, 2);
    	D = B * B - 4 * A * C;
    	if(D < 0) {
    	   return 0;
    	}

    	x1 = (-B + sqrt(D)) / (2 * A);
    	x2 = (-B - sqrt(D)) / (2 * A);
    	y1 = k * x1 + b;
    	y2 = k * x2 + b;
    	break;
    }


	float minX = fmin(mA.mPosX, mB.mPosX);
	float minY = fmin(mA.mPosY, mB.mPosY);
	float maxX = fmax(mA.mPosX, mB.mPosX);
	float maxY = fmax(mA.mPosY, mB.mPosY);

	if((x1 < minX) || (x1 > maxX)) {
		res -= 1;
	} else {
	    if((y1 < minY) || (y1 > maxY)) {
	        res -= 1;
	    }
	}

	if((x2 < minX) || (x2 > maxX)) {
		res -= 1;
	} else {
	    if((y2 < minY) || (y2 > maxY)) {
		   res -= 1;
	    }
	}
	return res;


}

utyg::Location Ball::getPos() {

	return mPos;
}

void Ball::setPos(utyg::Location pPos) {
	mPos.mPosX = pPos.mPosX;
    mPos.mPosY = pPos.mPosY;
    mPos.mRadius = pPos.mRadius;
    utyg::Log::info("PARAMS %f % f", mPos.mPosX, mPos.mPosY);
}


void Ball::setDeath() {
	mIsDead = true;
}

void Ball::setType(int pType) {
	mType = pType;
}



void Ball::draw() {
	 static GLfloat sphere_mat1[4]={1.0f, 1.0f, 1.0f, 1.0f};   //White
	 static GLfloat sphere_mat2[4]={0.0f, 0.0f, 1.0f, 1.0f};   //Blue
	 static GLfloat sphere_mat3[4]={0.0f, 1.0f, 0.0f, 1.0f};   //Green
	 static GLfloat sphere_mat4[4]={1.0f, 0.0f, 0.0f, 1.0f};   //Red

	 static GLfloat lightpos[4]={-1, -1, 0.0f, 0.0f};
	 GLfloat eqn1[4];
	 GLfloat eqn2[4];


	 eqn1[0] = 1;
	 eqn1[1] = 0;
	 eqn1[2] = 0;
	 eqn1[3] = 0.001;

	 eqn2[0] = -1;
	 eqn2[1] = 0;
	 eqn2[2] = 0;
	 eqn2[3] = 0.001;


	 glCullFace(GL_BACK);
	 glPushMatrix();
 	    glTranslatef(mPos.mPosX, mPos.mPosY, 200); //Переместим объект в нужное место

	    switch(mType) {   //Повернем объект
	    case(0):
	    case(1):
	        glRotatef(mRotateAngle, 1.0, 1.0, 0.0);
	    	break;
	    case(2):
		case(3):
	        glRotatef(mRotateAngle, 1.0, 1.0, 0.0);
	        break;
	    default:
	    	break;
        }

 		GLUquadric* l;  //Установим стиль для нашего шара
        l = new GLUquadric();
        gluQuadricDrawStyle(l, GLU_FILL);
        gluQuadricNormals(l, GLU_FLAT);

        //Устанавливаем цвет щариков

        if(mTotalCrash) {
        	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat4);
        } else {
        switch(mType) {
        case(0): // Белый в самом начале
		    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat1);
            break;
        case(1): // Синий в начале
   		    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat2);
            break;
        case(2):

        case(3):
            if(mIsDead) {
            	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat2);
            } else {
            	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat1);
            }
            break;
        default:
        	break;
        }
        }

        //gluCylinder(l, 10, 40, 60, 50, 50);

        //Зададим плоскость отсечени
        if(mType == 2) {
        	glEnable(GL_CLIP_PLANE0);
    	    glClipPlanef(GL_CLIP_PLANE0, eqn1);
        }
        if(mType == 3) {
        	glEnable(GL_CLIP_PLANE0);
            glClipPlanef(GL_CLIP_PLANE0, eqn2);
        }

        gluSphere(l, mPos.mRadius, 50, 50);



       // mRenderText->renderText("The Quick Brown Fox Jumps Over The Lazy Dog",
         //             0,   0,    1, 1);
        if((mType == 3) || (mType == 2)) {
            glPushMatrix();
                if(mType == 2) {
                     glCullFace(GL_FRONT);
                }
                glRotatef(90, 0.0, 1.0, 0.0);
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sphere_mat3);
                gluDisk(l, 0, mPos.mRadius, 20, 20);
            glPopMatrix();
         }
         glDisable(GL_CLIP_PLANE0);
         glDisable(GL_CLIP_PLANE1);
	glPopMatrix();

}
