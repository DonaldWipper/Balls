#include "SetBalls.hpp"
#include "Log.hpp"



   setBalls::setBalls(utyg::Context* pContext): mTimeService(pContext->mTimeService),
		   mInputService(pContext->mInputService)  {
       mMaxRadius = 0;
       mWidth = 0;
       mHeight = 0;
       utyg::Location *mPos = new utyg::Location();
       mPos->mPosX = 0;
       mPos->mPosY = 0;
       mTimeNewBall = 0;
       mLine = new SwypeLine(pContext);
       mInputService->setRefPoint(mPos);
       mTotalCrash = false;


   } 

   void setBalls::setParams(float pW, float pH) {
	   mMaxRadius = pW / 14;
	   mWidth = pW;
	   mHeight = pH;
   }

   //Make new from top
   void setBalls::reinit(int pNum) {
	   remove(pNum);
	   Ball lBall;
	   utyg::Location lPos;
	   while(true) {
		   lPos.mRadius = mMaxRadius + (rand() % 6) / 6.0 * mMaxRadius * 2.5;;
	       lPos.mPosX = lPos.mRadius + (rand() % 128) / 128.0 * (mWidth - lPos.mRadius * 2);
	       lPos.mPosY = mHeight + (rand() % 128) / 128.0 * mHeight;
	       bool res = false;
	       for(int i = 0; i < mBalls.size(); i++) {
	    	   if(i != pNum) {
	    		   if(!isEnoughDist(lPos, mBalls[i].getPos()))  {
	                   res = true;
	            	   break;
	               }
	           }
	       }
	       if(res == false) {
	    	  lBall.setPos(lPos);
	    	  mBalls.push_back(lBall);
	    	  break;
	       }
	   }
   }


   void setBalls::update() {
	   if(mTotalCrash) {
		   return;
	   }
	   float lTimeStep = mTimeService->elapsed();
	   mTimeNewBall += lTimeStep;
	   if(mTimeNewBall > 1) {
		   addNew();
		   mTimeNewBall = 0;
	   }
       mLine->update();
       std::vector<Ball>::iterator it = mBalls.begin();

       while(it != mBalls.end()) {
    	   if( ((*it).getPos().mPosY < -(*it).getPos().mRadius))
    	   {
    		   if((*it).getType() == 0) {
    			   (*it).setTotalCrash();
    	           mTotalCrash = true;
    		   }

    	   }


    	   if( ((*it).getPos().mPosY < -(*it).getPos().mRadius) ||
    		   ((*it).getPos().mPosX < -(*it).getPos().mRadius) ||
    		   ((*it).getPos().mPosX >  mWidth + (*it).getPos().mRadius))

    	   {
    		   it = mBalls.erase(it);
    	       continue;
    	   }


		   if(mLine->checkBallIntersect(*it)) {
			   //remove(i);
			   if((*it).getType() == 1) {
				   (*it).setTotalCrash();
				   mTotalCrash = true;
			   }

			   if((*it).getType() == 0) {
				   (*it).setDeath();
				   Ball lBallLeft = (*it), lBallRight = (*it);
				   it = mBalls.erase(it);
				   lBallLeft.setType(2);
				   lBallRight.setType(3);
				   it = mBalls.insert(it, lBallLeft);
				   it = mBalls.insert(it, lBallRight);
				   continue;

			   }
		   }

		   (*it).update();
		   it++;

	   }
   }



   void setBalls::draw() {
	   for(int i = 0; i < mBalls.size(); i++) {
		   if(mBalls[i].getPos().mPosY < mHeight +   mBalls[i].getPos().mRadius) {
		       mBalls[i].draw();
		   }
	   }
	   mLine->draw();
   }


   int setBalls::getCount() {
	   return mBalls.size();
   }

   Ball setBalls::getBall(int pNum) {
	   return mBalls[pNum];
   }



   bool setBalls::isEnoughDist(utyg::Location lPos1, utyg::Location lPos2) {
	   float lDist = sqrt(pow(lPos1.mPosX - lPos2.mPosX, 2) + pow(lPos1.mPosY - lPos2.mPosY, 2));
	   if(lDist < lPos1.mRadius + lPos2.mRadius) {
		   return false;
	   } else {
		   return true;
	   }
   }



   //Add new Ball
   void setBalls::addNew() {
	   Ball lBall;
	   utyg::Location lPos;

	   while(true) {
    	   lPos.mRadius = mMaxRadius + (rand() % 6) / 6.0 * mMaxRadius * 2.5;
           lPos.mPosX = lPos.mRadius  + (rand() % 128) / 128.0 * (mWidth - lPos.mRadius  * 2);
           lPos.mPosY = mHeight +  (rand() % 128) / 128.0 * mHeight;
           float res = false;
           for(int i = 0; i < mBalls.size(); i++) {
               if(!isEnoughDist(lPos, mBalls[i].getPos())) {
            	   res = true;
                   break;
               } 
           }
           if(res == false) {
        	   lBall.setPos(lPos);
        	   mBalls.push_back(lBall);
               break;
           }

       }
   }

   void setBalls::remove(int pNum) {
	   std::vector<Ball>::iterator it = mBalls.begin();
       if(it != mBalls.end()) {
           it += pNum;
           mBalls.erase(it);
       }

   }

   setBalls::~setBalls() {
       mBalls.clear();
    }


