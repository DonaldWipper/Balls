#ifndef _UTYG_CONTEXT_HPP_
#define _UTYG_CONTEXT_HPP_

#include "Types.hpp"


namespace utyg {
    class TimeService;
    class GraphicsService;
    class InputService;


    struct Context {
        GraphicsService* mGraphicsService;
        InputService*    mInputService;
        TimeService*     mTimeService;
    };
}
#endif
