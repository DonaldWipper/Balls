LOCAL_PATH := $(call my-dir)
MY_LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

NDK_MODULE_PATH := $(LOCAL_PATH)
LS_CPP=$(subst $(1)/,,$(wildcard $(1)/*.cpp))
LOCAL_MODULE    := shuttle
LOCAL_SRC_FILES := $(call LS_CPP,$(LOCAL_PATH)) 
LOCAL_LDLIBS    := -landroid -llog -lEGL -lGLESv1_CM 


LOCAL_STATIC_LIBRARIES := android_native_app_glue 

include $(BUILD_SHARED_LIBRARY)




$(call import-module,android/native_app_glue)



