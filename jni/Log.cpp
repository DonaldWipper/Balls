#include "Log.hpp"

#include <stdarg.h>
#include <android/log.h>

namespace utyg {
    void Log::info(const char* pMessage, ...) {
        va_list lVarArgs;
        va_start(lVarArgs, pMessage);
        __android_log_vprint(ANDROID_LOG_INFO, "utyg", pMessage,
            lVarArgs);
        __android_log_print(ANDROID_LOG_INFO, "utyg", "\n");
        va_end(lVarArgs);
    }

    void Log::error(const char* pMessage, ...) {
        va_list lVarArgs;
        va_start(lVarArgs, pMessage);
        __android_log_vprint(ANDROID_LOG_ERROR, "utyg", pMessage,
            lVarArgs);
        __android_log_print(ANDROID_LOG_ERROR, "utyg", "\n");
        va_end(lVarArgs);
    }

    void Log::warn(const char* pMessage, ...) {
        va_list lVarArgs;
        va_start(lVarArgs, pMessage);
        __android_log_vprint(ANDROID_LOG_WARN, "utyg", pMessage,
            lVarArgs);
        __android_log_print(ANDROID_LOG_WARN, "utyg", "\n");
        va_end(lVarArgs);
    }

    void Log::debug(const char* pMessage, ...) {
        va_list lVarArgs;
        va_start(lVarArgs, pMessage);
        __android_log_vprint(ANDROID_LOG_DEBUG, "utyg", pMessage,
            lVarArgs);
        __android_log_print(ANDROID_LOG_DEBUG, "utyg", "\n");
        va_end(lVarArgs);
    }
    void Log::utya(const char* pMessage, ...) {
            va_list lVarArgs;
            va_start(lVarArgs, pMessage);
            __android_log_vprint(ANDROID_LOG_UNKNOWN , "UTYA", pMessage,
                lVarArgs);
            __android_log_print(ANDROID_LOG_UNKNOWN , "UTYA", "\n");
            va_end(lVarArgs);
        }
}
