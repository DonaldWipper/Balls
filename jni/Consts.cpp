#include "Consts.hpp"

namespace utyg {
    extern const int NUM_BALLS = 0;
    extern const double NEW_POINT_TIME = 0.05;
    extern const double ERASE_POINT_TIME = 1;
    extern const double BALLS_MIN_SPEED = 4;
    extern const double BALLS_DELTA_SPEED = 2;

}
