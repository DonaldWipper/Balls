#ifndef _UTYG_TIMESERVICE_HPP_
#define _UTYG_TIMESERVICE_HPP_

#include "Types.hpp"

#include <time.h>

namespace utyg {
    class TimeService {
    public:
        TimeService();

        void reset();
        void update();

        double now();
        float elapsed();

    private:
        float mElapsed;
        double mLastTime;
    };
}
#endif
