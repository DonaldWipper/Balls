#ifndef _UTYG_LOG_HPP_
#define _UTYG_LOG_HPP_

namespace utyg {
    class Log {
    public:
        static void error(const char* pMessage, ...);
        static void warn(const char* pMessage, ...);
        static void info(const char* pMessage, ...);
        static void debug(const char* pMessage, ...);
        static void utya(const char* pMessage, ...);
    };
}

#ifndef NDEBUG
    #define utyg_Log_debug(...) utyg::Log::debug(__VA_ARGS__)
#else
    #define utyg_Log_debug(...)
#endif

#endif
