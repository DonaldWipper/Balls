#include "SwypeLine.hpp"
#include "Log.hpp"
#include <GLES/gl.h>
#include <GLES/glext.h>


SwypeLine::SwypeLine(utyg::Context* pContext): mTimeService(pContext->mTimeService),
		   mInputService(pContext->mInputService)  {
	mTimePrevPoint = 0;
	mTimeDraw = 0;

}



void SwypeLine::addPoint(utyg::Location pNewPoint) {
	 mLine.push_back(pNewPoint);
}


void SwypeLine::removePoint(int pNum) {
	 std::vector<utyg::Location>::iterator it = mLine.begin();
	 if(it != mLine.end()) {
	     it += pNum;
	     mLine.erase(it);
	 }
}


void SwypeLine::removeFirstPoints(int pCnt) {
	 std::vector<utyg::Location>::iterator it = mLine.begin();
	 for(int i = 0; i < pCnt; i++) {
	     if(it != mLine.end()) {
	         mLine.erase(it);
	     }
	 }
}

bool SwypeLine::checkBallIntersect(Ball mBall) {
	double res = 0;
	if(mLine.size() == 0) {
		return false;
	}
	for(int i = 0; i < mLine.size() - 1; i++) {
		utyg::Location lLeft, lRight;
		lLeft.mPosX = mLine[i].mPosX;
		lLeft.mPosY = mLine[i].mPosY;
		lRight.mPosX = mLine[i + 1].mPosX;
		lRight.mPosY = mLine[i + 1].mPosY;
		utyg::Log::info("INET %d", res);

		res += mBall.checkIntersectLine(lLeft, lRight);
		utyg::Log::info("RESS %d", res);
	}
	if(res == 2) {
        return true;
	} else {
		return false;
	}
	return false;
}


void SwypeLine::remove() {
	mLine.clear();
}

void SwypeLine::update() {
	 float lTimeStep = mTimeService->elapsed();
     mTimePrevPoint += lTimeStep;
	 mTimeDraw += lTimeStep;

	 if(mInputService->ifStartTouch() == true) {
	     if(mTimePrevPoint > utyg::NEW_POINT_TIME) {
		     utyg::Location lPos;
			 lPos.mPosX = mInputService->getHorizontal();
	         lPos.mPosY = mInputService->getVertical();
	         addPoint(lPos);
	         mTimePrevPoint = 0;
		 }
	 } else {
		if(mLine.size() != 0) {
		    remove();
		}
		mTimeDraw = 0;
		mTimePrevPoint = 0;
	 }
	 if (mTimeDraw > utyg::ERASE_POINT_TIME) {
	     int lNumErase = (mTimeDraw - utyg::ERASE_POINT_TIME) / utyg::NEW_POINT_TIME;
	     removeFirstPoints(lNumErase);
	     mTimeDraw -= lNumErase *  utyg::NEW_POINT_TIME;
	 }
}



void SwypeLine::draw() {
	int N = mLine.size() * 3;
	utyg::Log::info("SIZE %d", N);
    float mPoints[N];
	for(int i = 0; i < N / 3; i++) {
	    mPoints[3 * i] = mLine[i].mPosX;
		mPoints[3 * i + 1] = mLine[i].mPosY;
		mPoints[3 * i + 2] = 10;
	}
	glEnableClientState(GL_VERTEX_ARRAY);
	glColor4f(0.0, 1.0,0, 0.0);
	glVertexPointer(3, GL_FLOAT, 0, mPoints);
	glLineWidth(3);
	glDrawArrays(GL_LINE_STRIP, 0, N / 3);
	glDisableClientState(GL_VERTEX_ARRAY);
}
