#ifndef _UTYG_INPUTHANDLER_HPP_
#define _UTYG_INPUTHANDLER_HPP_

#include <android/input.h>

namespace utyg {
    class InputHandler {
    public:
        virtual ~InputHandler() {};

        virtual bool onTouchEvent(AInputEvent* pEvent) = 0;
    };
}
#endif
