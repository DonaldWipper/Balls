#ifndef _UTYG_GAMEMANAGER_HPP_
#define _UTYG_GAMEMANAGER_HPP_

#include "ActivityHandler.hpp"
#include "Context.hpp"
#include "GraphicsService.hpp"
#include "TimeService.hpp"
#include "Types.hpp"
#include "SetBalls.hpp"


namespace shtl {
    class GameManager : public utyg::ActivityHandler {
    public:
        GameManager(utyg::Context* pContext);


    protected:
        utyg::status onActivate();
        void onDeactivate();
        utyg::status onStep();

        void onStart();
        void onResume();
        void onPause();
        void onStop();
        void onDestroy();

        void onSaveState(void** pData, size_t* pSize);
        void onConfigurationChanged();
        void onLowMemory();

        void onCreateWindow();
        void onDestroyWindow();
        void onGainFocus();
        void onLostFocus();

    private:
        float mTimePrev;
        float mTimeDeath; //How long plane was dead
        utyg::GraphicsService* mGraphicsService;
        utyg::InputService*    mInputService;
        utyg::TimeService*     mTimeService;
        setBalls *mSetBalls;
   };
}
#endif
