#ifndef _GRAPHFIGURES_setBalls_HPP_
#define _GRAPHFIGURES_setBalls_HPP_

#include <math.h>
#include <vector>
#include "Types.hpp"
#include "Ball.hpp"
#include "InputService.hpp"
#include "Consts.hpp"
#include "TimeService.hpp"
#include "SwypeLine.hpp"


/*Class to control of asteroid's set
 * to delete
 * to add
 * to update
 * to draw
 */

class setBalls {
    	float mWidth;
    	float mHeight;
    	utyg::TimeService* mTimeService;
    	utyg::InputService* mInputService;
    	SwypeLine *mLine;
    	float mTimeNewBall;


    public:
         bool mTotalCrash;
    	 void setParams(float pW, float pH);
    	 setBalls();
    	 setBalls(utyg::Context *pContext);
    	 ~setBalls();

         float mMaxRadius;
         std::vector<Ball> mBalls;


         bool isEnoughDist(utyg::Location lPos1, utyg::Location lPos2); // If asteroid doesn't intersect others

         Ball getBall(int pNum);


         int getCount();
         void addNew();
         void reinit(int pNum);
         void remove(int pNum);
         void update();
         void drawLine(std::vector<utyg::Location> mLine);
         void draw();

 };



#endif
