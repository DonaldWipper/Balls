/*
 * Consts.h
 *
 *  Created on: 16 июля 2014 г.
 *      Author: dmitry
 */

#ifndef CONSTS_H_
#define CONSTS_H_

namespace utyg {
    extern const int NUM_BALLS;
    extern const double NEW_POINT_TIME;
    extern const double ERASE_POINT_TIME;
    extern const double BALLS_MIN_SPEED;
    extern const double BALLS_DELTA_SPEED;
}


#endif /* CONSTS_H_ */
