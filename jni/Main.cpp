#include "Context.hpp"
#include "GameManager.hpp"
#include "EventLoop.hpp"
#include "GraphicsService.hpp"
#include "InputService.hpp"
#include "TimeService.hpp"

//android_native_app_glue- модуль связи
//низкоуровнего кода и java-класса NativeActivity


//Точка входа в низкоуровневый код
void android_main(android_app* pApplication) {

	//Cоздаем сервисы, необходимые при использовании игры
    utyg::TimeService lTimeService;  //Контроль времени
    utyg::GraphicsService lGraphicsService(pApplication,
        &lTimeService); //Графический сервис, который тоже использует время
    utyg::InputService lInputService(pApplication,
        lGraphicsService.getWidth(), lGraphicsService.getHeight());
   
    // Fills the context.
    utyg::Context lContext = { &lGraphicsService, &lInputService,

&lTimeService};

    //Начинаем игоровой цикл

    utyg::EventLoop lEventLoop(pApplication);
    shtl::GameManager lGameManager(&lContext);
    lEventLoop.run(&lGameManager, &lInputService);
}
