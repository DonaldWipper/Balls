#include "GameManager.hpp"
#include "Log.hpp"

namespace shtl {
    GameManager::GameManager(utyg::Context* pContext) :
        mGraphicsService(pContext->mGraphicsService),
        mInputService(pContext->mInputService),
        mTimeService(pContext->mTimeService)
    {
            utyg::Log::info("Creating GameManager");
            srand(time(0));
            mSetBalls = new setBalls(pContext);
            mGraphicsService->initBalls(mSetBalls);
            mTimePrev = 0;

    }

    utyg::status GameManager::onActivate() {
        utyg::Log::info("Activating GameManager");

        // Starts services.
        if (mGraphicsService->start() != utyg::STATUS_OK) {
            return utyg::STATUS_KO;
        }

        mTimeService->reset();

        return utyg::STATUS_OK;
    }

    void GameManager::onDeactivate() {
        utyg::Log::info("Deactivating GameManager");
        mGraphicsService->stop();
    }

    utyg::status GameManager::onStep() {
    	float lTimeStep = mTimeService->elapsed();

    	mTimeService->update();
        if(mTimePrev > 3) {
        	return utyg::STATUS_EXIT;
        }
    	if(mSetBalls->mTotalCrash) {
        	mTimePrev += lTimeStep;
        }

        mSetBalls->update();


        if (mGraphicsService->update() != utyg::STATUS_OK) {
            return utyg::STATUS_KO;
        }
        return utyg::STATUS_OK;
    }

    void GameManager::onStart() {
        utyg::Log::info("onStart");
    }

    void GameManager::onResume() {
        utyg::Log::info("onResume");
    }

    void GameManager::onPause() {
        utyg::Log::info("onPause");
    }

    void GameManager::onStop() {
        utyg::Log::info("onStop");
    }

    void GameManager::onDestroy() {
        utyg::Log::info("onDestroy");
    }

    void GameManager::onSaveState(void** pData, size_t* pSize) {
        utyg::Log::info("onSaveInstanceState");
    }

    void GameManager::onConfigurationChanged() {
        utyg::Log::info("onConfigurationChanged");
    }

    void GameManager::onLowMemory() {
        utyg::Log::info("onLowMemory");
    }

    void GameManager::onCreateWindow() {
        utyg::Log::info("onCreateWindow");
    }

    void GameManager::onDestroyWindow() {
        utyg::Log::info("onDestroyWindow");
    }

    void GameManager::onGainFocus() {
        utyg::Log::info("onGainFocus");
    }

    void GameManager::onLostFocus() {
        utyg::Log::info("onLostFocus");
    }
}
