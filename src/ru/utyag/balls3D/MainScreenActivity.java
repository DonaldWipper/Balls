package ru.utyag.balls3D;
import ru.utyag.balls3D.R;
import android.app.Activity;
import android.app.NativeActivity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;


public class  MainScreenActivity extends Activity implements OnClickListener {
	Button btnStart;
	Button btnExit;
    
	
	
	public void onCreate(Bundle savedInstanteState) {
		super.onCreate(savedInstanteState);
		this.setContentView(R.layout.mainscreen);
	    btnStart = (Button) findViewById(R.id.btn_start);
	    btnExit = (Button) findViewById(R.id.btn_exit);
	    btnStart.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startNativeActivity(); 
				
			}
		});
	    
	    btnExit.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				
			}
		});
	    
	}
	
	public void startNativeActivity() {
		Intent intent = new Intent(this, NativeActivity.class);
	    startActivity(intent);	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	

	
	
}